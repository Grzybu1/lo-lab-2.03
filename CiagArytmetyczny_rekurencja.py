# Do pliku tekstowego proszę zapisać listę linii, które się wykonują w kolejności wykonania.
# Jeżeli dana linia wykonuje się kilka razy powinna znaleźć się na liście klika razy.
# Przykładowe rozwiązanie: 3, 5, 6, 5, 6, 10

def wyraz_ciagu (numer_wyrazu):
    global roznica_ciagu
    global wyraz_poczatkowy

    wynik = wyraz_poczatkowy
    if (numer_wyrazu > 1):
        wynik = wyraz_ciagu(numer_wyrazu - 1) + roznica_ciagu
    else:
        wynik = 1

    return wynik

roznica_ciagu = 2
wyraz_poczatkowy = 5

wartosc = wyraz_ciagu(2)
print(3, wartosc)
